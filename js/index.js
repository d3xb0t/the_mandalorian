$(function () {
    $('[data-toggle="popover"]').popover({placement : 'top'});
    $('.carousel').carousel({interval: 2000});
    $('#myModal').on('show.bs.modal', function(e){
      console.log('El modal contacto se esta mostrando');
      $('#btnModal').removeClass('btn-outline-success');
      $('#btnModal').prop('disabled', true);
      });
    $('#myModal').on('shown.bs.modal', function(e){
      console.log('El modal contacto se ha mostrado');
      });
    $('#myModal').on('hide.bs.modal', function(e){
      console.log('El modal contacto se esta ocultando');
      });
    $('#myModal').on('hidden.bs.modal', function(e){
      console.log('El modal contacto se ha ocultado');
      $('#btnModal').prop('disabled', false);
      });
  });